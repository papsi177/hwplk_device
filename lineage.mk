#
# Copyright (C) 2012 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# WARNING: Everything listed here will be built on ALL platforms,
# including x86, the emulator, and the SDK.  Modules must be uniquely
# named (liblights.tuna), and must build everywhere, or limit themselves
# to only building on ARM if they include assembly. Individual makefiles
# are responsible for having their own logic, for fine-grained control.

# Inherit device configuration
$(call inherit-product, device/huawei/HWPLK/full_HWPLK.mk)

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Correct boot animation size for the screen
DEVICE_SCREEN_HEIGHT := 1920
DEVICE_SCREEN_WIDTH := 1080

CM_BUILDTYPE := PsxProjectAD

PRODUCT_GMS_CLIENTID_BASE := android-huawei

PRODUCT_NAME := lineage_HWPLK
BOARD_VENDOR := huawei

